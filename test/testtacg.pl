#!/usr/bin/env perl 

# this script runs tacg thru several tests to see if it at least passes a light 
# stress test without bonking

# still to be tested:  
#     --HTML (this needs some more work to add HTML support for those
#             stanzas that have been added since I stopped work on the Web 
#             stuff
#     -R (alt pattern file)
#     different input formats
#     -x
#     
#     
require 5.005;
my $md5found;
BEGIN {eval "use Digest::MD5"; $md5found = $@ ? 0 : 1} 
# $md5found = 0;  # uncomment to simulate no MD5
if ($md5found == 0) { 
    print "\nOoops! Digest::MD5 module not found - continuing with simpler error checking\n\n" ;
} else {
    print "\nGood! Digest::MD5 module found ... continuing with test\n\n";
}

use strict;
#use Digest::MD5; 
$ENV{TACGLIB} = "../Data";
# or read in the data to verify that the analysis ran as expected using 
# an eval

my $tacg =       "../tacg";
my $seq =        "../Seqs/hlef.seq";
my $fastaseq =   "../Seqs/hsp.cluster.1000.fasta";
my $REBASE =     "../Data/rebase.data";
my $regex =      "../Data/regex.data";
my $rulefile =   "../Data/rules.data";
my $matrixfile = "../Data/matrix.data";
my $cmdline;
my $cmdline1;
my $MD5_hash;
my $GCC = `gcc --version |grep -i gcc`;

#my $hostname = `hostname`;
#print "local host name = $hostname\n";
my %wcTable = (
"raw" => " 98 317 7348 out", #wc
"raw_infile_fasta" => " 111 386 8401 out", #wc
"rev" => " 488 1774 38991 out", #wc
"comp" => " 496 1762 39639 out", #wc
"revcomp" => " 481 1772 38424 out", #wc
"notics_strands_numstart" => " 3436 61048 480458 out", #wc
"notics_strands_numstart_infile_fasta" => " 2996 23439 378525 out", #wc
"overhang_5" => " 94 401 3802 out", #wc
"overhang_3" => " 62 257 2360 out", #wc
"overhang_0" => " 88 364 3608 out", #wc
"Fragments_0" => " 43795 148479 3469348 out", #wc
"Fragments_1" => " 47707 181450 3634647 out", #wc
"Fragments_2" => " 47707 181450 3634645 out", #wc
"Fragments_3" => " 51619 214421 3799944 out", #wc
"FASTA_Fragments_0" => " 7200 26114 388901 out", #wc
"FASTA_Fragments_1" => " 10586 41959 485045 out", #wc
"FASTA_Fragments_2" => " 10586 41959 485019 out", #wc
"FASTA_Fragments_3" => " 13972 57804 581163 out", #wc
"begin_end_0" => " 43583 146263 3452794 out", #wc
"begin_end_1" => " 19890 79806 1842045 out", #wc
"begin_end_2" => " 16992 76293 1812986 out", #wc
"begin_end_3" => " 1992 9078 202900 out", #wc
"begin_end_4" => " 7085 38171 933740 out", #wc
"wide_ladder_map" => " 187 977 22918 out", #wc
"extract_hindiii_bamhi_0" => " 204 1884 14173 out", #wc
"extract_regex_0" => " 178 1637 13117 out", #wc
"extract_hindiii_bamhi_1" => " 148 1337 9977 out", #wc
"extract_regex_1" => " 96 843 6626 out", #wc
"extract_hindiii_bamhi_2" => " 160 1451 10737 out", #wc
"extract_regex_2" => " 86 748 5873 out", #wc
"regex_alt1" => " 334 3119 25293 out", #wc
"regex_FILE" => " 137 984 7845 out", #wc
"patterns_errors_0" => " 16 95 584 out", #wc
"patterns_errors_1" => " 21 126 805 out", #wc
"patterns_errors_2" => " 60 479 3286 out", #wc
"rule" => " 89 645 5802 out", #wc
"rulefile" => " 2707 17363 183737 out", #wc
"matrix_test_76" => " 833 7238 50060 out", #wc
"matrix_test_79" => " 570 4897 33868 out", #wc
"matrix_test_81" => " 482 4105 28408 out", #wc
"matrix_test_85" => " 331 2716 18791 out", #wc
"matrix_test_86" => " 301 2452 16956 out", #wc
"matrix_test_87" => " 289 2344 16222 out", #wc
"matrix_test_88" => " 234 1856 12849 out", #wc
"matrix_test_89" => " 204 1632 11230 out", #wc
"matrix_test_90" => " 189 1508 10375 out", #wc
"matrix_test_91" => " 186 1473 10133 out", #wc
"matrix_test_92" => " 167 1339 9158 out", #wc
"matrix_test_93" => " 65 448 3013 out", #wc
"matrix_test_95" => " 33 176 1144 out", #wc
"matrix_test_98" => " 24 127 789 out", #wc
"matrix_test_99" => " 24 125 775 out", #wc
"dam_dcm_test" => " 1713 13634 98089 out", #wc
"dcm_test" => " 1915 15847 116914 out", #wc
"clone_1" => " 257 1144 13704 out", #wc
"clone_2" => " 18 110 674 out", #wc
"clone_3" => " 4 23 141 out", #wc
"cost_10" => " 66 316 1965 out", #wc
"cost_25" => " 27 219 1613 out", #wc
"cost_75" => " 41 210 1261 out", #wc
"graphics_test_X" => " 203 2001 13960 out", #wc
"graphics_test_Y" => " 353 2001 14412 out", #wc
"graphics_test_L" => " 354 2002 14566 out", #wc
"Codon_tables_0" => " 196 2339 15377 out", #wc
"Codon_tables_1" => " 149 1762 11574 out", #wc
"Codon_tables_2" => " 489 6259 42577 out", #wc
"Codon_tables_3" => " 337 4182 28453 out", #wc
"Codon_tables_4" => " 489 6259 42576 out", #wc
"Codon_tables_5" => " 417 4926 34320 out", #wc
"Codon_tables_6" => " 337 4182 28453 out", #wc
"Codon_tables_7" => " 332 4103 27945 out", #wc
"Codon_tables_8" => " 196 2339 15375 out", #wc
"Codon_tables_9" => " 196 2339 15375 out", #wc
"Codon_tables_10" => " 484 6180 42066 out", #wc
"Codon_tables_11" => " 586 6724 48833 out", #wc
"Codon_tables_12" => " 265 3222 21417 out", #wc
"Codon_tables_13" => " 15 83 499 out", #wc
"Codon_tables_14" => " 15 83 499 out", #wc
"Codon_tables_15" => " 15 83 499 out", #wc
"Codon_tables_16" => " 15 83 499 out", #wc
"topology" => " 30 240 1534 out", #wc
"Proximity_1" => " 33 139 1232 out", #wc
"Proximity_2" => " 36 153 1536 out", #wc
"Proximity_3" => " 33 146 1232 out", #wc
"silent_short" => " 398 1550 31696 out", #wc
"silent_long" => " 1310 5434 105571 out", #wc
"ORF1_short_orfmap" => " 556 7328 48302 out", #wc
"ORF2_medium" => " 71 635 4178 out", #wc
"ORF3_long_orfmap" => " 92 626 5223 out", #wc
"ORF6xS_44_orfmap" => " 217 2513 17527 out", #wc
"ORF6xL_44_orfmap" => " 1795 27371 181975 out", #wc
"ORF6xL_150_orfmap" => " 83 669 6030 out", #wc
);

my %MD5_table = (
"raw" => "ccf790625524591e0e77cac5335db7c6", #MD5
"raw_infile_fasta" => "67311f223a9420f2b7a9409c60aecedb", #MD5
"rev" => "89c888189cd919b488d3e93f7359da18", #MD5
"comp" => "74e873dabccad295e36eba1d11bd7e83", #MD5
"revcomp" => "593c70cead000cd7e1154ac74b456a03", #MD5
"notics_strands_numstart" => "514bc9f79334ad0c49e1fa4d0ad81a29", #MD5
"notics_strands_numstart_infile_fasta" => "1850b7bca34fe10a50b24dc6b1ad3a62", #MD5
"overhang_5" => "0d1fd6c68d2bd7c2d2a1fbe83e743a17", #MD5
"overhang_3" => "a88cb283e5e9d15ad5b518da1caea2e8", #MD5
"overhang_0" => "1c0212e84863b55d13ebb1c915c6f369", #MD5
"Fragments_0" => "a9f950e2bcbf60277dc710f35b1ba1cc", #MD5
"Fragments_1" => "a115afdda292dfe5fe12112a0fb050d0", #MD5
"Fragments_2" => "b247ce7028f920268c78dbcd5f88f560", #MD5
"Fragments_3" => "8f2d5cbef34cf06ba901cbc60f8648f5", #MD5
"FASTA_Fragments_0" => "9567327bd594e6a2b95eed5e997280c1", #MD5
"FASTA_Fragments_1" => "94ee04a95a340b0276df49684ba002a9", #MD5
"FASTA_Fragments_2" => "97973d5ea4cc715f3b575dab7ed9b8ab", #MD5
"FASTA_Fragments_3" => "bb8a2fa34d26a3f8d14e475d258f54b4", #MD5
"begin_end_0" => "a1ac4403eeee901a7c22b36b603fe7b8", #MD5
"begin_end_1" => "d53c7c24275cc99ef9c1257f972d315c", #MD5
"begin_end_2" => "12ce4054fe5ad607eac33d79ad3a8d50", #MD5
"begin_end_3" => "6ad78927105cdcb096a844e0ca70fd61", #MD5
"begin_end_4" => "6ed5379b3017e46bf16ed54f3cb6c91b", #MD5
"wide_ladder_map" => "a7450de9e114faf2a46b9a70ec6e3ea3", #MD5
"extract_hindiii_bamhi_0" => "fe2ec59454784895ad0e0d5f1bab6817", #MD5
"extract_regex_0" => "2471c77a788bef66e41cd18bbdd03afe", #MD5
"extract_hindiii_bamhi_1" => "7c2b333e8050843e627140d1e2aa173b", #MD5
"extract_regex_1" => "ace8261f3479647dc4324ed8cbb0d93e", #MD5
"extract_hindiii_bamhi_2" => "3eaace0f58c8d2bbb0bdb839e2a80480", #MD5
"extract_regex_2" => "43c0edd50665aa87dbc1eda3b6efe05c", #MD5
"regex_alt1" => "fae9a8bca2637406c7888000eae621e3", #MD5
"regex_FILE" => "b6a8d2ea579d916930229a7280e35e25", #MD5
"patterns_errors_0" => "e1e1cf76ac56f1528bdcc601ec19eace", #MD5
"patterns_errors_1" => "8e2491ecd84bacb2e80da433cb23be91", #MD5
"patterns_errors_2" => "9bc9005d28578d6ad45922c5095532f6", #MD5
"rule" => "982910690fd870a40692c8e3779409f4", #MD5
"rulefile" => "4ad60e8e59ebc3cc5017be066c5cced2", #MD5
"matrix_test_76" => "235a0e7fc07615da5e390c13ec081ed1", #MD5
"matrix_test_79" => "8e1e87f4abc2bb3e7ff691a81244d3cb", #MD5
"matrix_test_81" => "2984a141486ec02ff584f024c333b1c1", #MD5
"matrix_test_85" => "b7ec13a445a48b0893a9c32b3da4cb50", #MD5
"matrix_test_86" => "fe10faaa0f285f17c21fe873546c5191", #MD5
"matrix_test_87" => "210af7ca09c890a62209cde02e411fb4", #MD5
"matrix_test_88" => "17a7fc28dc5d2253f1a8a9c8782c8cde", #MD5
"matrix_test_89" => "22e8caff2e4e35d01b2262c3b1ee36de", #MD5
"matrix_test_90" => "8a1b4a43177909690f9aad00f04af09b", #MD5
"matrix_test_91" => "6d0d4ddf7b1ce991cb6077bffe036691", #MD5
"matrix_test_92" => "6af484a62ec24a7fe3e743fab8c0a3bf", #MD5
"matrix_test_93" => "5e83926774912509c807bffefd522663", #MD5
"matrix_test_95" => "a89692cc65fe9e108883858be0947fb0", #MD5
"matrix_test_98" => "47ede6e6602b6ad5627b052b8c4ed4af", #MD5
"matrix_test_99" => "0d211e95e57b32dc6846ed0b29dfab08", #MD5
"dam_dcm_test" => "c7fe528fbfb710438d3edcda2dfcaa1d", #MD5
"dcm_test" => "06f95280d9168f1e2db696529dd85f49", #MD5
"clone_1" => "41ffc05ed6ffbe2922c79f1fdc938aaf", #MD5
"clone_2" => "4a1fba6b519baf8bbf4dce172143a93b", #MD5
"clone_3" => "19f58dc34575e5c6d299fa5f9cfa52e1", #MD5
"cost_10" => "e18eaa3a57002a097f5ec4ab5cf9e38b", #MD5
"cost_25" => "e660616f4772209e3258601b0bceb553", #MD5
"cost_75" => "db36e3d33dc6d9aabf6355f325aee819", #MD5
"graphics_test_X" => "dd9db009ce9590b05fc2c73425dad5aa", #MD5
"graphics_test_Y" => "091e30ea02f34a3670a551a4d19eaaef", #MD5
"graphics_test_L" => "8092d082ed8acc11679a2be5657b1a4b", #MD5
"Codon_tables_0" => "b3878e83813a2204ee68505c78c0bcff", #MD5
"Codon_tables_1" => "c5a4a0b2425c33d0bb0d79adad3bf796", #MD5
"Codon_tables_2" => "540351a471e58359a714022a9bf87789", #MD5
"Codon_tables_3" => "7174a6799c87bda77719fd9c5a4057a7", #MD5
"Codon_tables_4" => "e5c88d3232a347c7355ac4f3d9138cd2", #MD5
"Codon_tables_5" => "242cd0d563d863b512f897d0f809f6d5", #MD5
"Codon_tables_6" => "5bfbf8d5bd37ab8e0ab8d2cafdae7726", #MD5
"Codon_tables_7" => "66006bcb55c0b9e7b468ff35cf62d62e", #MD5
"Codon_tables_8" => "964aedc5ff795ba38681e9ecd5f2fb1e", #MD5
"Codon_tables_9" => "9cd0aaf77f6ebc88f2f6b24f18127cbf", #MD5
"Codon_tables_10" => "43c7f2436ddf4f418067e7ae64aa72af", #MD5
"Codon_tables_11" => "e69d433d522873d430d2fc56d5152aa0", #MD5
"Codon_tables_12" => "3c795b7cfae39bbc1a2ff36d5c8d7fa8", #MD5
"Codon_tables_13" => "f37ded8ba814e24b13dfef848c6b94bc", #MD5
"Codon_tables_14" => "89056a79ef22009659efb83b58c6edaa", #MD5
"Codon_tables_15" => "a0a9279059e74716613e936cac8087f9", #MD5
"Codon_tables_16" => "7efd56ce48433733ea99f0dcb6d4db8e", #MD5
"topology" => "66455ff2c15e220a8e92371c1397f32f", #MD5
"Proximity_1" => "3cb9e714775b97c35753859dd28b2092", #MD5
"Proximity_2" => "bcdd77578856c5ac9182cb22d673b6c4", #MD5
"Proximity_3" => "c3d6bf3d72e33878049fb3ec434d170d", #MD5
"silent_short" => "abb541bf9a11463ed781a7e699e5b33c", #MD5
"silent_long" => "75d009775602d4f8d915c0f60ef9aff9", #MD5
"ORF1_short_orfmap" => "f4e3cf4997f94f7f635976948cfc9b22", #MD5
"ORF2_medium" => "9fb9ceb5316cce87b6cd7b5aff61be29", #MD5
"ORF3_long_orfmap" => "b5fa4d5be0d503d911b1ce9a2d1a1306", #MD5
"ORF6xS_44_orfmap" => "547d717791aaea7987b06c58e03f3cbf", #MD5
"ORF6xL_44_orfmap" => "4cae953813947177f5cbe7d4841a29a6", #MD5
"ORF6xL_150_orfmap" => "ee6789e6bce15ed79fca17243516ad88", #MD5
);

print << "POI";

Now running thru some automated tests of the tacg executable that
you built.. This will take as long as a few minutes to run, depending on the 
speed of your machine.  If it fails, please send the logfile back to me:
hjm\@tacgi.com

The signatures for these tests were generated with:
gcc (GCC) 3.3.3 (Debian 20040314)
on:
Linux bodi 2.4.19 #5 Fri Sep 12 15:07:39 PDT 2003 i686 GNU/Linux

The gcc you used to build tacg was probably:
$GCC

If they're different, certain tests may fail due to compiler differences
and there are also output differences between the same compiler/libs on 
different architectures.

Please check not only the MD5 signatures, but also the wc sigs if 
you're compiling on different compilers and different platforms.

Nevertheless, I'd like to know if things appear to bugger up.

<Hit a key to continue>

POI

my $tmp = <STDIN>;

open(LOG, ">test.log") or die "Can't open logfile 'test.log': $!";  


my @begin = (1,   765,   22989,  77673,  101798);
my @end   = (0, 77673,  101798,  86735,       0);
my @matrix_cutoffs = (76, 79, 81, 85, 86, 87, 88, 89, 90, 91, 92, 93, 95, 98, 99);

print LOG "from:  ", `uname -a`, "\n";
print LOG "using:  ", `../tacg -v`, "\n";

# Test --raw
   $cmdline = "$tacg --raw  -L -e 333 < $seq > out";
   $MD5_hash = process_tacg_results($cmdline, "raw");

# Test -- raw w/ infile (remember - raw will read WHOLE file as raw, not just stanzas)
   $cmdline = "$tacg --raw  -L -e 333 --infile $fastaseq > out";
   $MD5_hash = process_tacg_results($cmdline, "raw_infile_fasta");

# Test --rev
   $cmdline = "$tacg --rev -L -e3333   < $seq > out";
   $MD5_hash = process_tacg_results($cmdline, "rev");

# Test --comp
   $cmdline = "$tacg --comp -L -e3333   < $seq > out";
   $MD5_hash = process_tacg_results($cmdline, "comp");
   
# Test --revcomp
   $cmdline = "$tacg --revcomp -L -e3333   < $seq > out";
   $MD5_hash = process_tacg_results($cmdline, "revcomp");


# Test --version - this will always fail on other machines.
#   $cmdline = "$tacg -v  >& out";
#   $MD5_hash = process_tacg_results($cmdline, "version");


# Test --notics & --strands 1 & numstart at a negative (also tests -T)
   $cmdline = "$tacg -M22 -lc -T3,1 -L --strands 1 --notics --numstart -3446  -b44444 -e99999 -w 120  < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "notics_strands_numstart");

 # Test --notics & --strands 1 & numstart at a negative (also tests -T) --infile & FASTA
   $cmdline = "$tacg -M22 -lc -T3,1 -L --strands 1 --notics --numstart -3446  -b44 -e888 -w 120  --infile $fastaseq  > out";
   $MD5_hash = process_tacg_results($cmdline, "notics_strands_numstart_infile_fasta");
  

# Test -o (overhang)
   $cmdline = "$tacg  -M4 -S -lc -o5 -b44444 -e99999  < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "overhang_5");

   $cmdline = "$tacg  -M4 -S -lc -o3 -b44444 -e99999  < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "overhang_3");

   $cmdline = "$tacg  -M4 -S -lc -o0 -b44444 -e99999  < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "overhang_0");

# Test Fragments
for (my $f=0; $f<4; $f++) {
   $cmdline = "$tacg -n 4  -Lc -S  -T 6,3 -F $f -O'123456x,100' < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "Fragments_$f");
}   

# Test Fragments & FASTA
for (my $f=0; $f<4; $f++) {
   $cmdline = "$tacg -n 4  -Lc -S  -T 6,3 -F $f -O'123456x,100' < $fastaseq  > out";
	$MD5_hash = process_tacg_results($cmdline, "FASTA_Fragments_$f");
}   

# Test begin/end with several options
for (my $i=0; $i<5; $i++) {
	my $width = 60 + ($i * 15);
   $cmdline = "$tacg -b $begin[$i] -e $end[$i] -n 4 -Lc -S -T 6,3  -w $width < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "begin_end_$i");
}   

#Test ladder Map (-l) - some occassional hiccups here on some platforms
$cmdline = "$tacg -w 120 -n6 -l < $seq  > out";
$MD5_hash = process_tacg_results($cmdline, "wide_ladder_map");

# Test extract (-X) option
for (my $i=0; $i<3; $i++) {
   $cmdline = "$tacg -b $begin[$i] -e $end[$i] -x'hindiii,bamhi'  -X 11,17,0 < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "extract_hindiii_bamhi_$i");
   
   $cmdline = "$tacg -b $begin[$i] -e $end[$i] --regex 'Rxtest:gc(cc|tac)nr{2,4}yt' -X 15,23,1 < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "extract_regex_$i");   
}   

# Test other regex ops
$cmdline = "$tacg  --regex 'Rxtest2:gyt{3,6}n{2,5}ymk(ty|ghy)' -X 15,23,1 < $seq  > out";
$MD5_hash = process_tacg_results($cmdline, "regex_alt1"); 

$cmdline = "$tacg  --regex 'FILE:$regex' -sl -S2 < $seq  > out";
$MD5_hash = process_tacg_results($cmdline, "regex_FILE"); 


# Test cmdline pattern flag (-p), with errors
for (my $i=0; $i<3; $i++) {
   $cmdline = "$tacg -b $begin[$i] -e $end[$i] -p 'testpat,gcwwgtgatr,$i' -S < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "patterns_errors_$i");
}   

# Test --rule
   $cmdline = "$tacg  --rule 'Rule1, \
   ((MwoI:1:8 & NaeI:1:8 & NarI:1:8) ^ ( NciI:1:8 & NcoI:1:8) & \
   (NdeI:1:8 ^ ((NgoMIV:1:8 & NheI:1:8)))),866' < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "rule");

# Test --rulefile
   $cmdline = "$tacg  --rulefile '$rulefile'  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "rulefile");

# Test matrix
   for (my $i=0; $i<15; $i++){
      my $co = $matrix_cutoffs[$i];
      $cmdline = "$tacg -# $co -R $matrixfile -S  -b 43747 -e 94321 < $seq  > out";
	   $MD5_hash = process_tacg_results($cmdline, "matrix_test_$co");
   }
# Test dam & dcm
   $cmdline = "$tacg --dam --dcm -n5 -S  -c < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "dam_dcm_test");

# Test -g (gel)
   $cmdline = "$tacg  --dcm -n5 -S2 -c -O123x,88 < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "dcm_test");

# Test --clone
   $cmdline = "$tacg  --clone '222_4444,5555_6666,44567_55555,55999_65888,4445x5554,6667x9999,11111x22222,88888x111111' < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "clone_1");
   
   $cmdline = "$tacg  --clone '222_4444,5555_6666,44567_55555,55999_65888,4445x5554,6667x9999,11111x22222,88888x111111'  -e 22221 < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "clone_2");

   $cmdline = "$tacg  --clone '222_4444,5555_6666,5557x6566,55999_65888,4445x5554,6667x9999,11111x22222,88888x111111' < $seq >& out";
   $MD5_hash = process_tacg_results($cmdline, "clone_3");

# Test --cost
   $cmdline = "$tacg  -S -M4 --cost 10 -b44444 -e99999  < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "cost_10");

   $cmdline = "$tacg  -s -m2 --cost 25 -b4344 -e9999  -w 120 < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "cost_25");

   $cmdline = "$tacg  -S 2 --cost 75 -b4344 -e9999  < $seq  > out";
   $MD5_hash = process_tacg_results($cmdline, "cost_75");

# Test -G (graphics)
my @opts = ("X", "Y", "L");
for (my $i=0; $i<3; $i++) {
   $cmdline = "$tacg -b 60000 -e 90000 -pjjj,mmmmmm,0 -G 200,$opts[$i]   < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "graphics_test_$opts[$i]");
}

# Test -C (Codons)
for (my $i=0; $i<17; $i++) {
	my $start = $i +1;
   $cmdline = "$tacg  -b $start -e 10000 -C $i -O 123456x,44  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "Codon_tables_$i");
}   

# Test topology - this one buggers up - diffs in 'diff'?

   $cmdline  = "$tacg -f0 -n5 -e 33333  -S1 < $seq  > out1; ";
   $cmdline .= "$tacg -f1 -n5 -e 33333  -S1 < $seq  > out2; ";
   $cmdline .= "diff out1 out2 > out ";
#   system('$cmdline; $cmdline1; diff out out1 > diff.f0.f1; mv diff.f0.f1 out');
  $MD5_hash = process_tacg_results($cmdline, "topology");

# Test -P (Proximity)
   $cmdline = "$tacg -b 5000 -e 25000 -P'hindiii,-l500,bamhi'  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "Proximity_1");

   $cmdline = "$tacg -b 26000 -e 56000 -P'hindiii,+200-500,bamhi'  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "Proximity_2");

   $cmdline = "$tacg -b 40000 -e 55000 -P'hindiii,-200-700,bamhi'  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "Proximity_3");

# Test --silent
   $cmdline = "$tacg -e 550 --silent -LT1,1  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "silent_short");

   $cmdline = "$tacg -b 550 -e 2535 --silent -LT1,1  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "silent_long");

# Test --ps & --pdf
# if (-x "/usr/bin/gs" || -x $ENV{TACG_GS_BIN}) {
   
#    $cmdline = "$tacg  -n6 -M2 --ps -e 10000 < $seq  > out; mv tacg_Map.ps out";
# 	$MD5_hash = process_tacg_results($cmdline, "postscript_short");
# 
#    $cmdline = "$tacg  -n6 -M2 --ps -e 50000 < $seq  > out; mv tacg_Map.ps out";
# 	$MD5_hash = process_tacg_results($cmdline, "postscript_long");
# 
#    $cmdline = "$tacg  -n6 -M2 --pdf -e 10000 < $seq  > out; mv tacg_Map.pdf out";
# 	$MD5_hash = process_tacg_results($cmdline, "PDF_short");
# 
#    $cmdline = "$tacg  -n5 -M2 --pdf -e 50000 -O123456,55 < $seq  > out; mv tacg_Map.pdf out";
# 	$MD5_hash = process_tacg_results($cmdline, "PDF_long");
# }	else {
# 	print "You don't have an executable ghostscript installed at '/usr/bin/gs.'\n",
#    "Skipping the postscript & PDF test.  If it's installed elsewhere,\n",
#    "symlink it to '/usr/bin/gs'\n";
# }

# Test -O

   $cmdline = "$tacg -b 5000 -e 25000 -O123456x,30  --orfmap < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "ORF1_short_orfmap");

   $cmdline = "$tacg  -e 5000 -O123456x,60  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "ORF2_medium");

   $cmdline = "$tacg  -O123x,150  --orfmap < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "ORF3_long_orfmap");
	
   $cmdline = "$tacg   -b 14 -e 10000 -w 90 -O 123456x,44  --orfmap  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "ORF6xS_44_orfmap");
	
   $cmdline = "$tacg   -b 14  -w 134 -O 123456x,44  --orfmap  < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "ORF6xL_44_orfmap");
	
   $cmdline = "$tacg  -O123x,150 -w 120 --orfmap < $seq  > out";
	$MD5_hash = process_tacg_results($cmdline, "ORF6xL_150_orfmap");

   print << "CLEANUP";
   
   The output for --ps and --pdf are not tested here as they both 
   reference the date which will change and the PDF conversion seems to be 
   quite variable.
   
      If you have problems with the postscript output, remember that the
   new output is APPENDED to any current file named 'tacg_Map.ps'.
      Also you may have font incompatibilities or missing fonts which may 
   result in the ghostscript interpreter crashing as well.
      Also, the postscript routines are the least well-debugged so you 
   might just be hitting a true bug..
      To verify the PDF and postscript output, take a look at the resulting 
   pages with 'gv' or the acrobat reader.


   Don't forget to clean out the test directory after running the tests 
   as they may generate a huge amount of data that you probably will never 
   need again.  The only thing you might need again is the 'testtacg.pl' 
   script, and the '.err' files - the output of the run if it doesn't 
   match with what I've determinied to the REAL output on my dev machine.
   If you're concerned about the mismatch, please mail me (hjm\@tacgi.com)
   to ask if I need the output and we can arrange the exchange.
   
   Incidentally, this test took ~32s to run my dev machine (IBM thinkpad a22p
   - 900MHz PentiumIII/Coppermine).  You can time yours by invoking it as:
   'rm tacg; make; time make test'
   
CLEANUP


sub process_tacg_results {
	 my ($cmdline) = shift;
    my ($testtype) = shift;
	 my $return_value = system($cmdline);
    my $file = "out";
    my $hash = "";
    my $wc = "";
#   print "return_value = $return_value \n";
   if ($return_value == 1) {
      print LOG "Failed commandline = $cmdline\n";
      print "FAILED!\n";
   } else {
      open(FILE, "out") or die "Can't open 'out': $!"; 
      binmode(FILE); 
      $wc = `wc out`;
#      print "wc before: [$wc]\n";
      $wc =~ s/\s+/\ /g;
      chomp $wc;chop $wc;
#      print " wc after: [$wc]\n\n";

   #      print LOG "wc of out = $wc\n";
      if ($md5found == 1) { 
         $hash = Digest::MD5->new->addfile(*FILE)->hexdigest ;
      } else {
         $hash = "UNDEFINED";
      }
      print LOG "\"$testtype\" => ", "\"$hash\", #MD5\n\"$testtype\" => ", "\"$wc\", #wc\n$cmdline\n\n";
      #,"

   }
   if ($md5found == 1) { 
      if ( $MD5_table{$testtype} eq $hash ) {
         print "passed test (MD5): $testtype\n";
      } else {
         print "\n!!FAILED!! test: $testtype\n";
         print "the MD5 sig: $hash \n  should be: $MD5_table{$testtype}\n";
         if ($wc ==$wcTable{$testtype}) {
             print "however the wordcount matches:\n";
         } else {
             print "and the wordcount is off as well:\n";
         }
         print "the wc sig: $wc \n should be: $wcTable{$testtype}\n";
         print "cmd: $cmdline \n\n";
         my $errfile = "$testtype" . ".MD5.err";
         system("mv out $errfile");
      }
   } else {
      if ($wcTable{$testtype} eq $wc) {
         print "passed test (wc): $testtype\n";
      } else {
         print "\n!!FAILED!! test: $testtype\n";
         print "the wc sig: [$wc] \n should be: [$wcTable{$testtype}]\n";
         print "cmd: $cmdline \n\n";
         my $errfile = "$testtype" . ".wc.err";
         system("mv out $errfile");

      }
   }
   return $hash;
}
